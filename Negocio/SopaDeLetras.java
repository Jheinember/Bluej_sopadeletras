package Negocio;


/**
 * Write a description of class SopaDeLetras here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaDeLetras implements InterfazMatriz
{
    // instance variables - replace the example below with your own
    private char sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras()
    {
        
    }

    public SopaDeLetras(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }
        
     //Crear la matriz con las correspondientes filas:
     
     String palabras2[]=palabras.split(",");
     this.sopas=new char[palabras2.length][];
     int i=0;
     for(String palabraX:palabras2)
     {
         //Creando las columnas de la fila i
         this.sopas[i]=new char[palabraX.length()];
         pasar(palabraX,this.sopas[i]);
         i++;
        
     }
     
     
    }
    
    private void pasar (String palabra, char fila[])
    {
    
        for(int j=0;j<palabra.length();j++)
        {
            fila[j]=palabra.charAt(j);
        }
    }
    
    
    public String toString()
    {
    String msg="";
    for(int i=0;i<this.sopas.length;i++)
    {
        for (int j=0;j<this.sopas[i].length;j++)
        {
            msg+=this.sopas[i][j]+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    public String toString2()
    {
    String msg="";
    for(char filas[]:this.sopas)
    {
        for (char dato :filas)
        {
            msg+=dato+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    
    public boolean esCuadrada()
    {
        boolean cuadrada = false;
        
        if(sopa.length==sopa[0].length){                     
          cuadrada=true;
        }else
        {
          cuadrada=false;
        }   
        
        return cuadrada;
    }
    
    
     public boolean esDispersa()
    {
        return false;
    }
    
    public boolean esRectangular()
    {
    
        boolean rectangular = false;
        
        if(sopa.length==sopa[0].length){                     
          rectangular=false;
        }else
        {
          rectangular=true;
        }   
        
        return rectangular;
    }
    
    
    
    /*
        retorna cuantas veces esta la palabra en la matriz
       */
    public char getContar(String palabra)
    {
        for (int x=0;x<sopa.length;x++) {
             
          for (int y=0;y<sopa.length;y++) {
              
              if (sopa[x]==sopa[y])
              System.out.println("El número "+sopa[x]+" está repetido");
          }
        }    
        
        return 'o';
    }
    
    
    /*
        debe ser cuadrada sopas
       */
    public char []getDiagonalPrincipal() throws Exception
    {
         boolean msg= false;
         char mensaje;   
         msg= esCuadrada(msg);
         if(msg == true)
         mensaje='s';
         else
         throw new Exception("No se puede crear la sopa");
        return null;
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopas*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
    
    public int getCantCol(int fila){
        
      if(sopa==null || sopa.length>=fila || fila>0)
        return -1;
     else
        return sopa[fila].length;
     }
     
     
     public int filaMasDatos(int fila){
       
             int  suma = sopa.length;   
             for(int i=0;i < sopa.length; i++){   
                 for(int j=0;j < sopa[0].length; j++){  
                     fila = sopa[i][j];   
                    }//fin for columnas  
                }//fin for filas   
                 return fila; 
        } 
//!
}
